#!/bin/bash


###############################################################################
#                           Challenge 1.1                                     #
###############################################################################

###############################################################################
# Description:  Automate IAM roles creation and for common AWS managed policies
#               and generate md document with all the commands
# https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html
#
# - Developer power user
# - Network administator
# - Read-Only
# - Security auditor
# - Support User
# - System Administrator
# - View-Only
#
###############################################################################

# 1-Retrieve ARN

# 2-Define file names
TEMPLATE_POLICY_FILE="assume-role-policy-template.json"
POLICY_FILE="assume-role-policy.json"
COMMANDS_FILE_NAME="Challenge1.1_CLI_commands.md"
ROLE_NAME=""
ROLE_TYPE=""

# Job functions: https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html
JOB_FUNCTIONS=(
  job-function/DatabaseAdministrator
  job-function/NetworkAdministrator
  job-function/SystemAdministrator
  job-function/ViewOnlyAccess
  job-function/DataScientist
  job-function/SupportUser
  job-function/Billing
  AdministratorAccess
  PowerUserAccess
  ReadOnlyAccess
  SecurityAudit
)

# 3-Generate policy file from template with principal ARN
sed -e "s|PRINCIPAL_ARN|${PRINCIPAL_ARN}|g" ${TEMPLATE_POLICY_FILE} > ${POLICY_FILE}

# 4-Write commands to be run in the CLI
write_commands_to_run_in_cli(){
  ROLE_NAME="rbtsb_$(get_valid_role_name ${1})Role"
  ROLE_TYPE=${1}

  echo "### To set up ${1} Role run:" >> $COMMANDS_FILE_NAME
  write_backthick_block
  # 1-To create role and specify assume policy
  echo  "ROLE_ARN=\$(aws iam create-role --role-name ${ROLE_NAME} --assume-role-policy-document file://${POLICY_FILE} --output text --query Role.Arn)" >> $COMMANDS_FILE_NAME

  # 2-Attach policy to role (see ROLE_TYPEs in https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html)
  echo "aws iam attach-role-policy --role-name ${ROLE_NAME} --policy-arn arn:aws:iam::aws:policy/${ROLE_TYPE}" >> $COMMANDS_FILE_NAME
  write_backthick_block

  echo "Check by running:" >> $COMMANDS_FILE_NAME
  # 3-Chek role and policy
  write_backthick_block
  echo "aws sts assume-role --role-arn \${ROLE_ARN} --role-session-name ${ROLE_NAME}" >> $COMMANDS_FILE_NAME
  write_backthick_block
  echo "If you want to delete this role run:" >> $COMMANDS_FILE_NAME
  
  # 4-Delete if necessary
  write_backthick_block
  echo "aws iam detach-role-policy --role-name ${ROLE_NAME} --policy-arn arn:aws:iam::aws:policy/${ROLE_TYPE}" >> $COMMANDS_FILE_NAME
  echo "aws iam delete-role --role-name ${ROLE_NAME}" >> $COMMANDS_FILE_NAME
  write_backthick_block
}

get_valid_role_name(){
  policy_name=${1}
  if [[ ${policy_name} == *"/"* ]]; then
    echo $(remove_policy_function_from_policy_name ${policy_name})
  else
    echo ${policy_name}
  fi
}

remove_policy_function_from_policy_name(){
  untrimmed_policy_name=${1}
  IFS='/'
  read -a starr <<< "${untrimmed_policy_name}"
  echo "${starr[1]}"
}


write_markdown_file_headers(){
cat > $COMMANDS_FILE_NAME <<- 'EOM'
# Quickly roles and policy set up:
Set up the roles and attach the policies to them by running next commands:
```
PRINCIPAL_ARN=$(aws sts get-caller-identity --query Arn --output text)
```
EOM
}

write_backthick_block(){
cat >> $COMMANDS_FILE_NAME <<- 'EOM'
```
EOM
}

write_markdown_file_headers

# 4-Execute the functions for the roles
for job_function in "${JOB_FUNCTIONS[@]}"; do
  write_commands_to_run_in_cli ${job_function}
done
