#!/bin/bash


###############################################################################
#                           Challenge 1.1                                     #
###############################################################################

###############################################################################
# Description:  Automate IAM roles creation and for common AWS managed policies
#               and generate md document with all the commands
# https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html
#
# - Developer power user
# - Network administator
# - Read-Only
# - Security auditor
# - Support User
# - System Administrator
# - View-Only
#
###############################################################################

# 1-Retrieve ARN
PRINCIPAL_ARN=$(aws sts get-caller-identity --query-Arn --output text)

# 2-Define file names
TEMPLATE_POLICY_FILE="assume-role-policy-template.json"
POLICY_FILE="assume-role-policy.json"
ROLE_NAME=""
ROLE_TYPE=""

# Job functions: https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html
JOB_FUNCTIONS=(
  job-function/DatabaseAdministrator
  job-function/NetworkAdministrator
  job-function/SystemAdministrator
  job-function/ViewOnlyAccess
  job-function/DataScientist
  job-function/SupportUser
  job-function/Billing
  AdministratorAccess
  PowerUserAccess
  ReadOnlyAccess
  SecurityAudit
)

# 3-Generate policy file from template with principal ARN
sed -e "s|PRINCIPAL_ARN|${PRINCIPAL_ARN}|g" ${TEMPLATE_POLICY_FILE} > ${POLICY_FILE}

# 4-Write commands to be run in the CLI
create_and_assume_iam_role(){
  ROLE_NAME="rbtsb_$(get_valid_role_name ${1})Role"
  ROLE_TYPE=${1}

  echo "Setting up ${1} Role run as ${ROLE_NAME}:"
  # 1-To create role and specify assume policy
  ROLE_ARN=$(aws iam create-role --role-name ${ROLE_NAME} --assume-role-policy-document file://${POLICY_FILE} --output text --query Role.Arn)

  # 2-Attach policy to role (see ROLE_TYPEs in https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html)
  aws iam attach-role-policy --role-name ${ROLE_NAME} --policy-arn arn:aws:iam::aws:policy/${ROLE_TYPE}

}

get_valid_role_name(){
  policy_name=${1}
  if [[ ${policy_name} == *"/"* ]]; then
    echo $(remove_policy_function_from_policy_name ${policy_name})
  else
    echo ${policy_name}
  fi
}

remove_policy_function_from_policy_name(){
  untrimmed_policy_name=${1}
  IFS='/'
  read -a starr <<< "${untrimmed_policy_name}"
  echo "${starr[1]}"
}


# 4-Execute the functions for the roles
for job_function in "${JOB_FUNCTIONS[@]}"; do
  create_and_assume_iam_role ${job_function}
done
