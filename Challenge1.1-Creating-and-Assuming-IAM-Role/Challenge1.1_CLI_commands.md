# Quickly roles and policy set up:
Set up the roles and attach the policies to them by running next commands:
```
PRINCIPAL_ARN=$(aws sts get-caller-identity --query Arn --output text)
```
### To set up job-function/DatabaseAdministrator Role run:
```
ROLE_ARN=$(aws iam create-role --role-name rbtsb_DatabaseAdministratorRole --assume-role-policy-document file://assume-role-policy.json --output text --query Role.Arn)
aws iam attach-role-policy --role-name rbtsb_DatabaseAdministratorRole --policy-arn arn:aws:iam::aws:policy/job-function/DatabaseAdministrator
```
Check by running:
```
aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name rbtsb_DatabaseAdministratorRole
```
If you want to delete this role run:
```
aws iam detach-role-policy --role-name rbtsb_DatabaseAdministratorRole --policy-arn arn:aws:iam::aws:policy/job-function/DatabaseAdministrator
aws iam delete-role --role-name rbtsb_DatabaseAdministratorRole
```
### To set up job-function/NetworkAdministrator Role run:
```
ROLE_ARN=$(aws iam create-role --role-name rbtsb_NetworkAdministratorRole --assume-role-policy-document file://assume-role-policy.json --output text --query Role.Arn)
aws iam attach-role-policy --role-name rbtsb_NetworkAdministratorRole --policy-arn arn:aws:iam::aws:policy/job-function/NetworkAdministrator
```
Check by running:
```
aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name rbtsb_NetworkAdministratorRole
```
If you want to delete this role run:
```
aws iam detach-role-policy --role-name rbtsb_NetworkAdministratorRole --policy-arn arn:aws:iam::aws:policy/job-function/NetworkAdministrator
aws iam delete-role --role-name rbtsb_NetworkAdministratorRole
```
### To set up job-function/SystemAdministrator Role run:
```
ROLE_ARN=$(aws iam create-role --role-name rbtsb_SystemAdministratorRole --assume-role-policy-document file://assume-role-policy.json --output text --query Role.Arn)
aws iam attach-role-policy --role-name rbtsb_SystemAdministratorRole --policy-arn arn:aws:iam::aws:policy/job-function/SystemAdministrator
```
Check by running:
```
aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name rbtsb_SystemAdministratorRole
```
If you want to delete this role run:
```
aws iam detach-role-policy --role-name rbtsb_SystemAdministratorRole --policy-arn arn:aws:iam::aws:policy/job-function/SystemAdministrator
aws iam delete-role --role-name rbtsb_SystemAdministratorRole
```
### To set up job-function/ViewOnlyAccess Role run:
```
ROLE_ARN=$(aws iam create-role --role-name rbtsb_ViewOnlyAccessRole --assume-role-policy-document file://assume-role-policy.json --output text --query Role.Arn)
aws iam attach-role-policy --role-name rbtsb_ViewOnlyAccessRole --policy-arn arn:aws:iam::aws:policy/job-function/ViewOnlyAccess
```
Check by running:
```
aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name rbtsb_ViewOnlyAccessRole
```
If you want to delete this role run:
```
aws iam detach-role-policy --role-name rbtsb_ViewOnlyAccessRole --policy-arn arn:aws:iam::aws:policy/job-function/ViewOnlyAccess
aws iam delete-role --role-name rbtsb_ViewOnlyAccessRole
```
### To set up job-function/DataScientist Role run:
```
ROLE_ARN=$(aws iam create-role --role-name rbtsb_DataScientistRole --assume-role-policy-document file://assume-role-policy.json --output text --query Role.Arn)
aws iam attach-role-policy --role-name rbtsb_DataScientistRole --policy-arn arn:aws:iam::aws:policy/job-function/DataScientist
```
Check by running:
```
aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name rbtsb_DataScientistRole
```
If you want to delete this role run:
```
aws iam detach-role-policy --role-name rbtsb_DataScientistRole --policy-arn arn:aws:iam::aws:policy/job-function/DataScientist
aws iam delete-role --role-name rbtsb_DataScientistRole
```
### To set up job-function/SupportUser Role run:
```
ROLE_ARN=$(aws iam create-role --role-name rbtsb_SupportUserRole --assume-role-policy-document file://assume-role-policy.json --output text --query Role.Arn)
aws iam attach-role-policy --role-name rbtsb_SupportUserRole --policy-arn arn:aws:iam::aws:policy/job-function/SupportUser
```
Check by running:
```
aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name rbtsb_SupportUserRole
```
If you want to delete this role run:
```
aws iam detach-role-policy --role-name rbtsb_SupportUserRole --policy-arn arn:aws:iam::aws:policy/job-function/SupportUser
aws iam delete-role --role-name rbtsb_SupportUserRole
```
### To set up job-function/Billing Role run:
```
ROLE_ARN=$(aws iam create-role --role-name rbtsb_BillingRole --assume-role-policy-document file://assume-role-policy.json --output text --query Role.Arn)
aws iam attach-role-policy --role-name rbtsb_BillingRole --policy-arn arn:aws:iam::aws:policy/job-function/Billing
```
Check by running:
```
aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name rbtsb_BillingRole
```
If you want to delete this role run:
```
aws iam detach-role-policy --role-name rbtsb_BillingRole --policy-arn arn:aws:iam::aws:policy/job-function/Billing
aws iam delete-role --role-name rbtsb_BillingRole
```
### To set up AdministratorAccess Role run:
```
ROLE_ARN=$(aws iam create-role --role-name rbtsb_AdministratorAccessRole --assume-role-policy-document file://assume-role-policy.json --output text --query Role.Arn)
aws iam attach-role-policy --role-name rbtsb_AdministratorAccessRole --policy-arn arn:aws:iam::aws:policy/AdministratorAccess
```
Check by running:
```
aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name rbtsb_AdministratorAccessRole
```
If you want to delete this role run:
```
aws iam detach-role-policy --role-name rbtsb_AdministratorAccessRole --policy-arn arn:aws:iam::aws:policy/AdministratorAccess
aws iam delete-role --role-name rbtsb_AdministratorAccessRole
```
### To set up PowerUserAccess Role run:
```
ROLE_ARN=$(aws iam create-role --role-name rbtsb_PowerUserAccessRole --assume-role-policy-document file://assume-role-policy.json --output text --query Role.Arn)
aws iam attach-role-policy --role-name rbtsb_PowerUserAccessRole --policy-arn arn:aws:iam::aws:policy/PowerUserAccess
```
Check by running:
```
aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name rbtsb_PowerUserAccessRole
```
If you want to delete this role run:
```
aws iam detach-role-policy --role-name rbtsb_PowerUserAccessRole --policy-arn arn:aws:iam::aws:policy/PowerUserAccess
aws iam delete-role --role-name rbtsb_PowerUserAccessRole
```
### To set up ReadOnlyAccess Role run:
```
ROLE_ARN=$(aws iam create-role --role-name rbtsb_ReadOnlyAccessRole --assume-role-policy-document file://assume-role-policy.json --output text --query Role.Arn)
aws iam attach-role-policy --role-name rbtsb_ReadOnlyAccessRole --policy-arn arn:aws:iam::aws:policy/ReadOnlyAccess
```
Check by running:
```
aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name rbtsb_ReadOnlyAccessRole
```
If you want to delete this role run:
```
aws iam detach-role-policy --role-name rbtsb_ReadOnlyAccessRole --policy-arn arn:aws:iam::aws:policy/ReadOnlyAccess
aws iam delete-role --role-name rbtsb_ReadOnlyAccessRole
```
### To set up SecurityAudit Role run:
```
ROLE_ARN=$(aws iam create-role --role-name rbtsb_SecurityAuditRole --assume-role-policy-document file://assume-role-policy.json --output text --query Role.Arn)
aws iam attach-role-policy --role-name rbtsb_SecurityAuditRole --policy-arn arn:aws:iam::aws:policy/SecurityAudit
```
Check by running:
```
aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name rbtsb_SecurityAuditRole
```
If you want to delete this role run:
```
aws iam detach-role-policy --role-name rbtsb_SecurityAuditRole --policy-arn arn:aws:iam::aws:policy/SecurityAudit
aws iam delete-role --role-name rbtsb_SecurityAuditRole
```
