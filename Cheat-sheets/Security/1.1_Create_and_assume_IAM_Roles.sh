#!/bin/bash


###############################################################################
#                       Create and Assume IAM roles                           #
###############################################################################

# 1.1 IAM Roles:
# Find role types here: # https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html

# 1-Retrieve ARN
PRINCIPAL_ARN=$(aws sts get-caller-identity --query Arn --output text)
ROLE_NAME="MyCoolRoleName"
ROLE_TYPE="MyRoleType" 
TEMPLATE_POLICY_FILE="~/my_path/template-policy-file.json"
POLICY_FILE="~/my_path/policy-file.json"

# 2-Using sed command to replace your strings in the template file (don't forget to create the template file first!)
#   Check the template file here: https://github.com/AWSCookbook/Security/blob/main/101-Creating-and-Assuming-an-IAM-Role/assume-role-policy-template.json
sed -e "s|PRINCIPAL_ARN|${PRINCIPAL_ARN}|g" ${TEMPLATE_POLICY_FILE} > ${POLICY_FILE}

# 3-Create role and specify assume policy
ROLE_ARN=$(aws iam create-role --role-name ${ROLE_NAME} --assume-role-policy-document file://POLICY_FILE --output text --query Role.Arn)

# 4-Attach policy to role (see ROLE_TYPEs in https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html)
aws iam attach-role-policy --role-name ${ROLE_NAME} --policy-arn arn:aws:iam::aws:policy/${ROLE_TYPE}

# 5-Validation checking
aws sts assume-role --role-arn ${ROLE_ARN} --role-session-name ${ROLE_NAME}


