# rbatistab-AWSCookBookSandbox

This is a space to play with AWSCookBook from John Culkin and Mike Zazon and generate some fun scripts :)

## Getting started

Have you checked John Culkin and Mike Zazon's awesome book, [AWS Cookbook: Recipes for Success on AWS](https://www.amazon.com/AWS-Cookbook-Recipes-Success/dp/1492092606/ref=sr_1_1?crid=2RUYOSIL3I4T2&keywords=aws+cookbook&qid=1658888968&sprefix=awscookbook%2Caps%2C144&sr=8-1)?
It is a cool place to get resources to solve your "point" problem, you think of it like a cheat sheet on steroids, not only with the formula you need but with a concise explanation and the links to the propper documentation. You wont lack a source to consult any concept, neither have to spend a lot of time searching, the book literally has very good references to dive deep.
  
It is for sure that the best teacher you'll have is getting your hands dirty on an AWS project, however here is a "drill training" and some fun scripts to help automate some work :)

## Check the cheat sheets

Some chapters have instructions, but do are you one of those people that prefer a good'ol copy-paste rather than writting? The cheat sheet's could be combined with the book to quickly get your instructions personalized and running fast (also avoid those annoying typos).
Visit the `Cheat Sheets` foler and take what you need ;)


Without much further to say, let's enjoy the AWS ride!

## Chapter 1:
* Cheat sheet to create and assume IAM roles
* Quick script to automate the set up of common [AWS job functions](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_job-functions.html). Just run:
```
chmod +x 101-Creating-and-Assuming-an-IAM-Role/Challenge1.1_job_function_automator.sh
./101-Creating-and-Assuming-an-IAM-Role/Challenge1.1_job_function_automator.sh
```
* Quick script to generate the CLI commands to set up the job functions above (this contains how to delete them as well)
```
chmod +x 101-Creating-and-Assuming-an-IAM-Role/Challenge1.1_markdown_generator.sh
bash 101-Creating-and-Assuming-an-IAM-Role/Challenge1.1_markdown_generator.sh
```
